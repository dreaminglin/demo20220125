package demo.practise.com;

import java.util.Scanner;

/**
 * @author: linjunmin
 * @date: 2022/1/26 - 0:02
 * @Version 1.0
 */
public class Demo {

    private static Double APPLE_PRICE = new Double(8);
    private static Double STRAWBERRY_PRICE = new Double(13);
    private static Double MANGO_PRICE = new Double(20);


    public static void main(String args[]){

        Scanner input = new Scanner(System.in);

        System.out.println("开始计算苹果和草莓的总价格，请输入苹果的斤数：");
        Integer appleWeightA = input.nextInt();
        System.out.println("请输入草莓的斤数：");
        Integer strawberryWeightA = input.nextInt();
        Double resultA = Demo.calculateA(appleWeightA,strawberryWeightA);
        System.out.println("苹果和草莓的总价格是：resultA = " + resultA);

        System.out.println("开始计算苹果、草莓和芒果的总价格，请输入苹果的斤数：");
        Integer appleWeightB = input.nextInt();
        System.out.println("请输入草莓的斤数：");
        Integer strawberryWeightB = input.nextInt();
        System.out.println("请输入芒果的斤数：");
        Integer mangoB = input.nextInt();
        Double resultB = Demo.calculateB(appleWeightB,strawberryWeightB,mangoB);
        System.out.println("苹果、草莓和芒果的总价格是：resultB = " + resultB);

        System.out.println("草莓打8折，开始计算苹果、草莓和芒果的总价格，请输入苹果的斤数：");
        Integer appleWeightC = input.nextInt();
        System.out.println("请输入草莓的斤数：");
        Integer strawberryWeightC = input.nextInt();
        System.out.println("请输入芒果的斤数：");
        Integer mangoC = input.nextInt();
        Double resultC = Demo.calculateC(appleWeightC,strawberryWeightC,mangoC);
        System.out.println("苹果、草莓和芒果的总价格是：resultC = " + resultC);

        System.out.println("购物满 100 减 10 块，开始计算苹果、草莓和芒果的总价格，请输入苹果的斤数：");
        Integer appleWeightD = input.nextInt();
        System.out.println("请输入草莓的斤数：");
        Integer strawberryWeightD = input.nextInt();
        System.out.println("请输入芒果的斤数：");
        Integer mangoD = input.nextInt();
        Double originalD = Demo.calculateB(appleWeightD,strawberryWeightD,mangoD);
        Double resultD = originalD - Math.floor(originalD/100)*10;


        System.out.println("购物满 100 减 10 块，苹果、草莓和芒果的总价格是：resultD = " + resultD);

    }

    private static Double calculateA(Integer appleWeight, Integer strawberryWeight){

        Double sum = appleWeight*APPLE_PRICE +strawberryWeight*STRAWBERRY_PRICE;

        return sum;

    }

    private static Double calculateB(Integer appleWeight, Integer strawberryWeight, Integer mangoWeight){

        Double sum = appleWeight*APPLE_PRICE +strawberryWeight*STRAWBERRY_PRICE + mangoWeight*MANGO_PRICE;

        return sum;

    }

    private static Double calculateC(Integer appleWeight, Integer strawberryWeight, Integer mangoWeight){

        Double sum = appleWeight*APPLE_PRICE +strawberryWeight*STRAWBERRY_PRICE*0.8 + mangoWeight*MANGO_PRICE;

        return sum;

    }
}
